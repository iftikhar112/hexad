package gr.hexad.assigment.exceptions;

public class IncorrectPostPaymentPostProcessFailed extends Throwable {
    public IncorrectPostPaymentPostProcessFailed(String strategy){

        super("Exception process the strategy "+strategy);

    }
}
