package gr.hexad.assigment.exceptions;

public class IncorrectPaymentPostProcessorTypeExeption extends Exception{

   public IncorrectPaymentPostProcessorTypeExeption(String message){

        super(message);
    }

}
