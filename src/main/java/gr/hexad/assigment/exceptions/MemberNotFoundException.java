package gr.hexad.assigment.exceptions;

public class MemberNotFoundException extends Throwable {
    public MemberNotFoundException(){
        super("Member not found");

    }
}
