package gr.hexad.assigment.service;


import gr.hexad.assigment.core.PaymentPostProcessStrategy;
import gr.hexad.assigment.core.RuleConfigLoader;
import gr.hexad.assigment.dao.MemberDAO;
import gr.hexad.assigment.domain.Member;
import gr.hexad.assigment.domain.PaymentInfo;
import gr.hexad.assigment.exceptions.IncorrectPostPaymentPostProcessFailed;
import gr.hexad.assigment.exceptions.MemberNotFoundException;
import gr.hexad.assigment.factories.AppContext;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
/*
 * This is mediator class which is used to 
 * pass messages between objects
 * It is handling all the core logic 
 * from loading rules to process and post processing payment 
 * strategies
 * 
 * 
 * */
public class PaymentServiceImpl extends PaymentService {

    private RuleConfigLoader paymentPostProcessConfigLoader;



public PaymentServiceImpl(){
    paymentPostProcessConfigLoader = AppContext.getRulesConfigLoader();


}
    @Override
    public PaymentInfo createPayment(PaymentInfo info) {

        Member member = MemberDAO.getCustomer(info.getMemberInfo().getCustomerEmail());
        info.setMemberInfo(member);

        return info;

    }

    @Override
    public PaymentInfo processPayment(PaymentInfo info) {

        info.setPaymentProcessedDate(LocalDateTime.now());

        return info;
    }

    @Override
    public List<String> paymentPostProcess(PaymentInfo info) throws IncorrectPostPaymentPostProcessFailed, MemberNotFoundException, URISyntaxException {
       
        List<String> postProcessStatus = new ArrayList<>();
                String status =null;
        Path path = null;
       List<PaymentPostProcessStrategy> postProcessess= paymentPostProcessConfigLoader.loadPostProcessRules().get(info.getPaymentType());
        for(PaymentPostProcessStrategy process : postProcessess) {
            status = process.postProcess(info);
            postProcessStatus.add(status);
                   if(status==null){

                       throw   new IncorrectPostPaymentPostProcessFailed("Payment Post process strategy failed");
                   }


        }

       return postProcessStatus;
    }
}
