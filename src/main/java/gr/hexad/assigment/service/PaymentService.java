package gr.hexad.assigment.service;


import gr.hexad.assigment.domain.PaymentInfo;
import gr.hexad.assigment.exceptions.IncorrectPostPaymentPostProcessFailed;
import gr.hexad.assigment.exceptions.MemberNotFoundException;

import java.net.URISyntaxException;
import java.util.List;

public abstract class PaymentService {

    public abstract PaymentInfo createPayment(PaymentInfo info);

    public abstract PaymentInfo processPayment(PaymentInfo info);

    public abstract List<String> paymentPostProcess(PaymentInfo info) throws IncorrectPostPaymentPostProcessFailed, MemberNotFoundException, URISyntaxException;


}
