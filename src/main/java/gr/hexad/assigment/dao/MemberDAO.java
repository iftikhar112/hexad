package gr.hexad.assigment.dao;

import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import gr.hexad.assigment.domain.Member;
import gr.hexad.assigment.exceptions.MemberAlreadyExistException;
import gr.hexad.assigment.exceptions.MemberNotFoundException;

/**
 * 
 * This class is responsible creating updating and getting the Member that are saved 
 * 
 * 
 * */


public class MemberDAO {

	public static final Map<String, Member> CUSTOMER_DB = new ConcurrentHashMap<>();
	static {
		createCustomerDB();

	}

	private static void createCustomerDB() {

		if (CUSTOMER_DB.isEmpty()) {
			CUSTOMER_DB.put("uknown New Member",
					new Member(0, "unknown new Member", "uknown New Member", "H-NO 1 Street 123 dubai UAE"));
			CUSTOMER_DB.put("jackmau@hotmail.com",
					new Member(1, "Jack Anderson", "jackmau@hotmail.com", "H-NO 1 Street 123 Lahore Pakistan"));
			CUSTOMER_DB.put("billmaher@gmail.com",
					new Member(2, "Bill Maher", "billmaher@gmail.com", "H-NO 1 Street 123 Frankfurt Germany "));
			CUSTOMER_DB.put("rick@yahoo.com",
					new Member(3, "Rick Hays", "rick@yahoo.com", "H-NO 1 Street 123 Sharja UAE"));
			CUSTOMER_DB.put("iftikhar@hotmail.com",
					new Member(4, "Iftikhar Hussain", "iftikhar@hotmail.com", "H-NO 1 Street 123 Krachi Pakistan"));
			CUSTOMER_DB.put("simon@reddit.com",
					new Member(5, "Simmon Mills", "simon@reddit.com", "H-NO 1 Street 123 Abu Dhabi UAE"));
			CUSTOMER_DB.put("mhijab@gmail.com",
					new Member(6, "M Hijab", "mhijab@gmail.com", "H-NO 1 Street 123 Berlin Germany"));
			CUSTOMER_DB.put("carina@etisalat.com",
					new Member(7, "Carina Haden", "carina@etisalat.com", "H-NO 1 Street 123 Dehli India"));
			CUSTOMER_DB.put("ninerva@aws.com",
					new Member(8, "Minerva McGonagall ", "ninerva@aws.com", "H-NO 1 Street 123 London UK"));
			CUSTOMER_DB.put("irma@hotmail.com",
					new Member(9, "Irma Pince", "irma@hotmail.com", "H-NO 1 Street 123 Newyork USA"));
			CUSTOMER_DB.put("aura@yahoo.com",
					new Member(10, "Aurora Sinistra", "aura@yahoo.com", "H-NO 1 Street 123 Shanghais China"));

		}
	}

	public static int memberCount() {
		return CUSTOMER_DB.size();
	}

	public static Member getCustomer(String customerEmail) {
		if (CUSTOMER_DB.get(customerEmail) == null) {
			CUSTOMER_DB.get("uknown New Member");
		}
		return CUSTOMER_DB.get(customerEmail);

	}

	public Member addCustomer(Member member) throws MemberAlreadyExistException {
		if (CUSTOMER_DB.get(member.getCustomerEmail()) != null) {
			throw new MemberAlreadyExistException("Member with same email id already exists");
		}
		member.setCustomerID(CUSTOMER_DB.keySet().size());
		CUSTOMER_DB.put(member.getCustomerEmail(), member);
		member.setMembershipExpiry(LocalDate.now().plusMonths(1));
		return member;

	}

	public Member updateCustomer(String customerEmail) throws MemberNotFoundException {

		if (CUSTOMER_DB.get(customerEmail) != null) {
			Member member = CUSTOMER_DB.get(customerEmail);
			member.setMembershipExpiry(member.getMembershipExpiry().plusMonths(1));
			return member;
		}
		throw new MemberNotFoundException();
	}

}
