package gr.hexad.assigment.domain;


import gr.hexad.assigment.enums.PaymentType;

import java.time.LocalDateTime;

public class PaymentInfo {

    private PaymentType paymentType;
    private LocalDateTime paymentDate;
    private LocalDateTime paymentProcessedDate;
    private Member memberInfo;
    private double amount;
    public PaymentInfo(){
        this.paymentDate = LocalDateTime.now();
    }

    public LocalDateTime getPaymentProcessedDate() {
        return paymentProcessedDate;
    }

    public void setPaymentProcessedDate(LocalDateTime paymentProcessedDate) {
        this.paymentProcessedDate = paymentProcessedDate;
    }

    public final PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Member getMemberInfo() {
        return memberInfo;
    }

    public void setMemberInfo(Member memberInfo) {
        this.memberInfo = memberInfo;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
