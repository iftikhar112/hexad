package gr.hexad.assigment.domain;

import java.time.LocalDate;

public class Member {
    private int customerID;
    private String customerName;
    private String customerEmail;
    private LocalDate membershipExpiry;
    private String address;


    public Member(int customerID, String customerName, String customerEmail,String address) {
        this.customerID = customerID;
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.membershipExpiry=LocalDate.now();
        this.address = address;
    }

    public Member( String customerName, String customerEmail,String address) {
        this.customerID = customerID;
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.membershipExpiry=LocalDate.now();
        this.address = address;
    }

    public Member() {
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public LocalDate getMembershipExpiry() {
        return membershipExpiry;
    }

    public void setMembershipExpiry(LocalDate membershipExpiry) {
        this.membershipExpiry = membershipExpiry;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public String getAddress() {
        return this.address;
    }
    public void setAddress(String address){
        this.address = address;
    }
}
