package gr.hexad.assigment.core;

import gr.hexad.assigment.domain.PaymentInfo;

public interface TemplateLoader {

     String getTemplate(PaymentInfo info,String templateFormat);
}
