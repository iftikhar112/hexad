package gr.hexad.assigment.core;


import gr.hexad.assigment.enums.PaymentPostprocessTypes;
import gr.hexad.assigment.enums.PaymentType;
import gr.hexad.assigment.exceptions.IncorrectPaymentPostProcessorTypeExeption;
import gr.hexad.assigment.exceptions.MemberAlreadyExistException;
import gr.hexad.assigment.factories.AppContext;
import gr.hexad.assigment.util.AppConstants;
import gr.hexad.assigment.util.AppUtils;

import java.io.IOException;
import java.net.URI;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/*
 * Implementation of rule loader via text file 
 * the rules are present in resources/RulesConfig
 * i.e 
 * payment.type.physical.product=>payment.post.processor.generate.packing.slip
 * Payment Type(Application.properties contains the mapping of this text which 
 * is payment.type.physical.product=PHYSICAL_PRODUCT )
 * => Process Strategy (colon seperated in case of multiple rules)
 * Payment Type =a PostProcessStrategy1:PostProcessStrategy2
 * 
 * this is smple implementation instead of text file db or other consitent presisten
 * storage could be used just implement rule RuleConfigLoader interface
 * 
 * Each payment type is PaymentTypes enum if we want to add new payment type 
 * we just need to add payment type in PaymentTypes enum and then mapp the exact enum.name
 * in application properties and add it in rule config along with rules
 * i.e
 * in enum NEW_PAYMENT_TYPE
 * in application.properties new.payment.type = NEW_PAYMENT_TYPE
 * in RuleConfig.txt     new.payment.type=rule1:rule2.......
 * same if we want to add new Strategy for payment we need to add PaymentPostprocessTypes enum
 * i.e  NEW_POST_PROCESS_RULE in PaymentPostprocessTypes
 * in application.properties like defined above in paymenttype case
 * and ruleconfig file
 * 
 * same if we want to change the rule we just change the mapping in RuleConfig.txt file
 * 
 * */



public class TextFileConfigLoader implements RuleConfigLoader {


    public static final String RULES_STRATEGY_SEPARATOR = "=>";
    public static final String ACTION_SEPARATOR = ":";
   private Map<PaymentType, List<PaymentPostProcessStrategy>> postProcessRules = null;
    @Override
    public Map<PaymentType, List<PaymentPostProcessStrategy>> loadPostProcessRules() {
        if(postProcessRules!=null){
            return postProcessRules;
        }

        URI uri = null;
        try {
            uri = ClassLoader.getSystemResource(AppConstants.RULE_CONFIG_FILE).toURI();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Path ruleFilePath = Paths.get(uri);
        try (Stream<String> stream = Files.lines(ruleFilePath)) {


            postProcessRules = stream.map(line -> {

                String[] rule = line.split(RULES_STRATEGY_SEPARATOR);
                PaymentType paymentType = PaymentType.valueOf(AppUtils.getProperty(rule[0]));
                String[] strategies = rule[1].split(ACTION_SEPARATOR);
                List<PaymentPostProcessStrategy> actions = Stream.of(strategies).map(item ->
                        {
                            try {


                                return AppContext.getPostProcessStrategyInstance(PaymentPostprocessTypes.valueOf(AppUtils.getProperty(item)));


                            } catch (IncorrectPaymentPostProcessorTypeExeption | MemberAlreadyExistException incorrectPaymentPostProcessorTypeExeption) {
                                incorrectPaymentPostProcessorTypeExeption.printStackTrace();
                                return null;
                            }
                        }

                ).collect(Collectors.toList());

                return new AbstractMap.SimpleImmutableEntry<>(paymentType, actions);
            }).collect(Collectors.toList()).stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));


        } catch (IOException e) {
            e.printStackTrace();
        }


        return Collections.unmodifiableMap(postProcessRules);
    }
}
