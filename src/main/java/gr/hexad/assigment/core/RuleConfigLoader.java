package gr.hexad.assigment.core;

import gr.hexad.assigment.enums.PaymentType;

import java.util.List;
import java.util.Map;

public interface RuleConfigLoader {


    Map<PaymentType, List<PaymentPostProcessStrategy>> loadPostProcessRules() ;
}
