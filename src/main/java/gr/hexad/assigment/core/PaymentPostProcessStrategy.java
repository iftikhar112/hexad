package gr.hexad.assigment.core;


import gr.hexad.assigment.domain.PaymentInfo;
import gr.hexad.assigment.exceptions.MemberNotFoundException;

@FunctionalInterface
public interface PaymentPostProcessStrategy {

     String postProcess(PaymentInfo info) throws MemberNotFoundException;

}
