package gr.hexad.assigment.factories;

import gr.hexad.assigment.core.PaymentPostProcessStrategy;
import gr.hexad.assigment.core.RuleConfigLoader;
import gr.hexad.assigment.core.TemplateLoader;
import gr.hexad.assigment.core.TextFileConfigLoader;
import gr.hexad.assigment.dao.MemberDAO;
import gr.hexad.assigment.domain.Member;
import gr.hexad.assigment.enums.PaymentPostprocessTypes;
import gr.hexad.assigment.enums.PaymentType;
import gr.hexad.assigment.enums.TemplateTypes;
import gr.hexad.assigment.exceptions.IncorrectPaymentPostProcessorTypeExeption;
import gr.hexad.assigment.exceptions.MemberAlreadyExistException;
import gr.hexad.assigment.service.PaymentService;
import gr.hexad.assigment.service.PaymentServiceImpl;
import gr.hexad.assigment.util.AppUtils;

import java.text.MessageFormat;
import java.time.format.DateTimeFormatter;
import java.util.EnumMap;

import static gr.hexad.assigment.util.AppConstants.*;
/*
 * This is context which handle initialization logic of different 
 * component to enable DI in the application. 
 * 
 * 
 * 
 * 
 * 
 * */



public class AppContext {
	// context for caching and getting postprocess strategy
	private static final EnumMap<PaymentPostprocessTypes, PaymentPostProcessStrategy> postProcessContext = new EnumMap<>(
			PaymentPostprocessTypes.class);

	public static PaymentService paymentService = null;

	private static RuleConfigLoader configLoader;

	private static MemberDAO memberDAO;
	
	
	public static PaymentPostProcessStrategy getPostProcessStrategyInstance(PaymentPostprocessTypes type)
			throws IncorrectPaymentPostProcessorTypeExeption, MemberAlreadyExistException {

		try {
			postProcessContext.putIfAbsent(type, PaymentPostProcessFactory.getPaymentPostProcessStrategy(type));
			return postProcessContext.get(type);
		} catch (IncorrectPaymentPostProcessorTypeExeption incorrectPaymentPostProcessorTypeExeption) {
			throw incorrectPaymentPostProcessorTypeExeption;
		}

	}

	public static RuleConfigLoader getRulesConfigLoader() {

		if (configLoader == null) {
			configLoader = new TextFileConfigLoader();
		}
		return configLoader;

	}

	public static MemberDAO getMemberDAO() {
		if (memberDAO == null) {
			memberDAO = new MemberDAO();
		}
		return memberDAO;
	}

	public static PaymentService getPaymentService() {

		if (paymentService == null) {

			paymentService = new PaymentServiceImpl();
		}
		return paymentService;
	}
}
