package gr.hexad.assigment.factories;

import static gr.hexad.assigment.util.AppConstants.COMMISSION_RECEIPT_TEMPLATE;
import static gr.hexad.assigment.util.AppConstants.EMAIL_TEMPLATE;
import static gr.hexad.assigment.util.AppConstants.MEMBER_CREATE_UPDATE_MESSAGE_TEMPLATE;
import static gr.hexad.assigment.util.AppConstants.PACKING_SLIP_TEMPLATE;

import gr.hexad.assigment.core.PaymentPostProcessStrategy;
import gr.hexad.assigment.core.TemplateLoader;
import gr.hexad.assigment.dao.MemberDAO;
import gr.hexad.assigment.domain.Member;
import gr.hexad.assigment.enums.PaymentPostprocessTypes;
import gr.hexad.assigment.enums.PaymentType;
import gr.hexad.assigment.enums.TemplateTypes;
import gr.hexad.assigment.exceptions.IncorrectPaymentPostProcessorTypeExeption;
import gr.hexad.assigment.exceptions.MemberAlreadyExistException;
import gr.hexad.assigment.util.AppUtils;
import static gr.hexad.assigment.factories.TemplateFactory.getTemplate;
/*
 * This class contains the implementation PaymentPostProcessStrategy interface 
 * it uses strategy pattern to initialize strategy on runtime for based on 
 * configuration defined in post process strategy rule file
 * 
 * 
 * */

public class PaymentPostProcessFactory {
	
	 public static PaymentPostProcessStrategy getPaymentPostProcessStrategy(PaymentPostprocessTypes type) throws MemberAlreadyExistException, IncorrectPaymentPostProcessorTypeExeption {
	        PaymentPostProcessStrategy strategy = null;
	        switch (type) {
	            case ACTIVATE_MEMBER_SHIP:
	            case UPGRADE_MEMBER_SHIP:
	                strategy = (info) -> {
	                    MemberDAO memberDAO = AppContext.getMemberDAO();
	                    Member member = null;
	                    TemplateLoader templateLoader = null;
	                    if (info.getPaymentType() == PaymentType.NEW_MEMBERSHIP) {
	                      
								try {
									member = memberDAO.addCustomer(info.getMemberInfo());
								} catch (MemberAlreadyExistException e) {
									
									e.printStackTrace();
								}
						
	                        templateLoader = getTemplate(TemplateTypes.MEMBER_CREATE_TEMPLATE_MESSAGE);
	                    } else {
	                        member = memberDAO.updateCustomer(info.getMemberInfo().getCustomerEmail());
	                        templateLoader = getTemplate(TemplateTypes.MEMBER_UPDATE_TEMPLATE_MESSAGE);
	                    }
	                    info.setMemberInfo(member);

	                    return templateLoader.getTemplate(info, MEMBER_CREATE_UPDATE_MESSAGE_TEMPLATE);
	                };
	                break;

	            case GENERATE_PACKING_SLIP:
	                strategy = (info) -> {
	                	Member member = MemberDAO.getCustomer(info.getMemberInfo().getCustomerEmail());
	                	info.setMemberInfo(member);
	                    TemplateLoader templateLoader = getTemplate(TemplateTypes.PACKING_SLIP);
	                    String template =templateLoader.getTemplate(info,PACKING_SLIP_TEMPLATE);
	                    return template;
	                };
	                break;
	            case ACTIVATION_EMAIL:
	            case UPGRADATION_EMAIL:
	                strategy = (info) -> {
	                    TemplateLoader templateLoader = getTemplate(TemplateTypes.CREATE_MEMBER_EMAIL_TEMPLATE);
	                    String template = templateLoader.getTemplate(info, EMAIL_TEMPLATE);
	                    System.out.println(template);
	                    if (!AppUtils.sendEmail(info.getMemberInfo(), template)) {
	                        return null;
	                    }

	                    return template;
	                };
	                break;
	            case GENERATE_COMMISSION_PAYMENT:
	                strategy = (info) -> {
	                	Member member = MemberDAO.getCustomer(info.getMemberInfo().getCustomerEmail());
	                	info.setMemberInfo(member);
	                    TemplateLoader templateLoader = getTemplate(TemplateTypes.GENERATE_COMMISSION_TEMPLATE);
	                    String template = templateLoader.getTemplate(info, COMMISSION_RECEIPT_TEMPLATE);

	                    return template;
	                };
	                break;
	            case FREE_FIRST_VIDEO:
	                strategy = (info) -> {

	                    TemplateLoader templateLoader = getTemplate(TemplateTypes.PACKING_SLIP_WITH_FREE_FIRST_AID_VIDEO);
	                    String template= templateLoader.getTemplate(info,PACKING_SLIP_TEMPLATE);
	                    return template;
	                };
	                break;
	            case GENERATE_DUPLICATE_PACKING_SLIP:
	                strategy = (info) -> {
	                	Member member = MemberDAO.getCustomer(info.getMemberInfo().getCustomerEmail());
	                	info.setMemberInfo(member);
	                    TemplateLoader templateLoader = getTemplate(TemplateTypes.DUPLICATE_PACKING_SLIP);
	                    String template = templateLoader.getTemplate(info, PACKING_SLIP_TEMPLATE);
	                    return template;
	                };

	                break;

	            default:
	                throw new IncorrectPaymentPostProcessorTypeExeption("Payment Processor type has no implementation defined" +
	                        " please provide implementation for payment post processor or select other post processor");

	        }

	        return strategy;


	    }

}
