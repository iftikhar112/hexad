package gr.hexad.assigment.factories;

import static gr.hexad.assigment.util.AppConstants.COMMISSION_AMOUNT;
import static gr.hexad.assigment.util.AppConstants.COMMISSION_RECEIPT_HEADING;
import static gr.hexad.assigment.util.AppConstants.DUPLICATE_PACKING_SLIP_HEADING_TEXT;
import static gr.hexad.assigment.util.AppConstants.MEMBER_CREATE_SUCCESS_MESSAGE;
import static gr.hexad.assigment.util.AppConstants.MEMBER_EXPIRY;
import static gr.hexad.assigment.util.AppConstants.MEMBER_ID;
import static gr.hexad.assigment.util.AppConstants.MEMBER_NAME;
import static gr.hexad.assigment.util.AppConstants.MEMBER_SHIP_ACTIVATION_EMAIL_HEADING;
import static gr.hexad.assigment.util.AppConstants.MEMBER_SHIP_CREATE_GREETINGS;
import static gr.hexad.assigment.util.AppConstants.MEMBER_SHIP__UPDATE_GREETINGS;
import static gr.hexad.assigment.util.AppConstants.MEMBER_UPDATE_SUCCESS_MESSAGE;
import static gr.hexad.assigment.util.AppConstants.PACKING_SLIP_CUSTMER_ADDRESS_COLUMN_TEXT;
import static gr.hexad.assigment.util.AppConstants.PACKING_SLIP_CUSTOMER_NAME_COLUMN_TEXT;
import static gr.hexad.assigment.util.AppConstants.PACKING_SLIP_PAMENT_TYPE_COLUMN_TEXT;
import static gr.hexad.assigment.util.AppConstants.PACKING_SLIP_PAYMENT_DATE_COLUMN_TEXT;
import static gr.hexad.assigment.util.AppConstants.PACK_SLIP_RECEIPT_HEADING_TEXT;
import static gr.hexad.assigment.util.AppConstants.TOTAL_PRICE;

import java.text.MessageFormat;
import java.time.format.DateTimeFormatter;

import gr.hexad.assigment.core.TemplateLoader;
import gr.hexad.assigment.enums.PaymentType;
import gr.hexad.assigment.enums.TemplateTypes;
import gr.hexad.assigment.util.AppUtils;

public class TemplateFactory {
	public static TemplateLoader getTemplate(TemplateTypes templateType) {

        switch (templateType) {

            case MEMBER_CREATE_TEMPLATE_MESSAGE:
            case MEMBER_UPDATE_TEMPLATE_MESSAGE: {
                return (info, format) -> {
                    String template ;
                    if (info.getPaymentType() == PaymentType.NEW_MEMBERSHIP) {
                        template = String.format(format, MEMBER_CREATE_SUCCESS_MESSAGE, MEMBER_ID, info.getMemberInfo().getCustomerID(),
                                MEMBER_NAME, info.getMemberInfo().getCustomerName(), MEMBER_EXPIRY, info.getMemberInfo().getMembershipExpiry().toString());
                    } else {

                        template = String.format(format, MEMBER_UPDATE_SUCCESS_MESSAGE, MEMBER_ID, info.getMemberInfo().getCustomerID(),
                                MEMBER_NAME, info.getMemberInfo().getCustomerName(), MEMBER_EXPIRY, info.getMemberInfo().getMembershipExpiry().toString());
                    }
                    System.out.println(template);
                    return template;

                };


            }
            case CREATE_MEMBER_EMAIL_TEMPLATE:
            case UPDATE_MEMBER_EMAIL_TEMPLATE: {

                return (info, format) -> {
                    String heading = MessageFormat.format(MEMBER_SHIP_ACTIVATION_EMAIL_HEADING, info.getMemberInfo().getCustomerName());
                    String greetings = "";
                    if (info.getPaymentType() == PaymentType.NEW_MEMBERSHIP) {
                        greetings = MessageFormat.format(MEMBER_SHIP_CREATE_GREETINGS, info.getMemberInfo().getCustomerName());
                    } else {
                        greetings = MessageFormat.format(MEMBER_SHIP__UPDATE_GREETINGS, info.getMemberInfo().getCustomerName());
                    }

                    String template = String.format(format, heading, greetings, MEMBER_ID, info.getMemberInfo().getCustomerID()
                            , MEMBER_NAME, info.getMemberInfo().getCustomerName(), MEMBER_EXPIRY, info.getMemberInfo().getMembershipExpiry().toString());
                    return template;

                };


            }
            case GENERATE_COMMISSION_TEMPLATE:
                return (info, format) -> {
                    String template = String.format(format, COMMISSION_RECEIPT_HEADING, MEMBER_ID, info.getMemberInfo().getCustomerID()
                            , TOTAL_PRICE, info.getAmount(), COMMISSION_AMOUNT, AppUtils.calculateCommisionPayment(info.getAmount()));

                    return template;
                };
            case PACKING_SLIP:
            case DUPLICATE_PACKING_SLIP:
            case PACKING_SLIP_WITH_FREE_FIRST_AID_VIDEO: {

                return (info, format) -> {
                    String heading = PACK_SLIP_RECEIPT_HEADING_TEXT;
                    String optinalInfoTextForFreeVideo = "";
                    if (templateType.equals(TemplateTypes.DUPLICATE_PACKING_SLIP)) {
                        heading = DUPLICATE_PACKING_SLIP_HEADING_TEXT;
                    } else if (TemplateTypes.PACKING_SLIP_WITH_FREE_FIRST_AID_VIDEO.equals(templateType)) {
                        optinalInfoTextForFreeVideo = "%n%nFree Video Inside";

                    }

                    String template = String.format(format, heading, PACKING_SLIP_CUSTOMER_NAME_COLUMN_TEXT, info.getMemberInfo().getCustomerName(),
                            PACKING_SLIP_PAMENT_TYPE_COLUMN_TEXT, info.getPaymentType().getPaymentType(), PACKING_SLIP_PAYMENT_DATE_COLUMN_TEXT
                            , info.getPaymentDate().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME), TOTAL_PRICE, info.getAmount(),
                            PACKING_SLIP_CUSTMER_ADDRESS_COLUMN_TEXT, info.getMemberInfo().getAddress());
                    template += optinalInfoTextForFreeVideo;
                    return template;
                };
            }

            default:
                return (info, format) -> "";

        }


    }

}
