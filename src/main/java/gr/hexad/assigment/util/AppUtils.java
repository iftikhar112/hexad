package gr.hexad.assigment.util;

import gr.hexad.assigment.domain.Member;

import java.io.IOException;
import java.util.Properties;

public class AppUtils {

    private static final Properties prop = new Properties();


    static {

        try {
            prop.load(AppUtils.class.getClassLoader().getResourceAsStream("application.properties"));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }




    public static String getProperty(String propertyName){

        return prop.getProperty(propertyName);
    }

    public static boolean sendEmail(Member member ,String template) {
        return true;
    }



public static double calculateCommisionPayment(double amount){
    double commision = 0.00;
    if(amount>0){
        commision = amount*AppConstants.COMMISSION_RATE;

    }
    return commision;

}

public static void addLineSeperator() {
    System.out.printf("%n%n"+new String(new char[50]).replace("\0", "-")+"\n");

	
}
}