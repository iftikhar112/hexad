package gr.hexad.assigment.util;

public class AppConstants {

    public static final String RULE_CONFIG_FILE="RulesConfig.txt";
    public static final String MEMBER_CREATE_SUCCESS_MESSAGE="Member created successfully";
    public static final String MEMBER_UPDATE_SUCCESS_MESSAGE="Member updated successfully";
    public static final String MEMBER_ID="Member ID";
    public static final String MEMBER_NAME="Member Name";
    public static final String MEMBER_EXPIRY="Member Expiry";
    public static final String MEMBER_CREATE_UPDATE_MESSAGE_TEMPLATE = "%n%n%30s.\n%-15s:%-15s\n%-15s:%s\n%-15s:%-15s\n";
    public static final String MEMBER_SHIP_ACTIVATION_EMAIL_HEADING= "Congratulation {0} ";
    public static final String MEMBER_SHIP_CREATE_GREETINGS = "Hi, {0} your membership has been created see the below details" ;
    public static final String MEMBER_SHIP__UPDATE_GREETINGS = "Hi, {0} your membership has been updated successfully";
    public static final String EMAIL_TEMPLATE = "%n%n%50s\n%10s\n%-30s:%30s\n%-30s:%30s";
    public static final String PACK_SLIP_RECEIPT_HEADING_TEXT ="Packing Slip";
    public static final String PACKING_SLIP_CUSTOMER_NAME_COLUMN_TEXT="Customer Name";
    public static final String PACKING_SLIP_PAYMENT_DATE_COLUMN_TEXT="Payment Date";
    public static final String PACKING_SLIP_PAMENT_TYPE_COLUMN_TEXT="Item Type";
    public static final String PACKING_SLIP_CUSTMER_ADDRESS_COLUMN_TEXT="Delivery Address";
    public static final String PACKING_SLIP_TEMPLATE = "%n%n%30s%n%-15s:%-15s%n%-15s:%-15s%n%-15s:%15s%n%-15s:%.2f%n%-15s:%-15s";
    public static final double COMMISSION_RATE = 0.2;
    public static final String COMMISSION_RECEIPT_TEMPLATE = "%n%n%-30s%n%-15s:%-15s%n%-15s:%.2f%n%-15s:%.2f";
    public static final String COMMISSION_RECEIPT_HEADING = "Commission Payment Receipt";
    public static final String TOTAL_PRICE = "Total Price";
    public static final String COMMISSION_AMOUNT="Commission Amount";

    public static final String DUPLICATE_PACKING_SLIP_HEADING_TEXT = "Duplicate Receipt for Royalty Department";
}
