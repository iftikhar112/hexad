package gr.hexad.assigment.enums;

public enum PaymentType {
    PHYSICAL_PRODUCT("Physical Product"), BOOK("Book") ,NEW_MEMBERSHIP("New Membership"),
    MEMBERSHIP_UPGRDE("Membership Upgrade"),VIDEO_LEARNING_T_SKY("Video Learning Sky");

    String paymentType;


    PaymentType(String paymentType){
        this.paymentType = paymentType;

    }

    public String getPaymentType() {
        return this.paymentType;
    }

}
