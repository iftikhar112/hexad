package gr.hexad.assigment;

import gr.hexad.assigment.core.PaymentPostProcessStrategy;
import gr.hexad.assigment.core.RuleConfigLoader;
import gr.hexad.assigment.dao.MemberDAO;
import gr.hexad.assigment.domain.Member;
import gr.hexad.assigment.domain.PaymentInfo;
import gr.hexad.assigment.enums.PaymentPostprocessTypes;
import gr.hexad.assigment.enums.PaymentType;
import gr.hexad.assigment.exceptions.IncorrectPaymentPostProcessorTypeExeption;
import gr.hexad.assigment.exceptions.IncorrectPostPaymentPostProcessFailed;
import gr.hexad.assigment.exceptions.MemberNotFoundException;
import gr.hexad.assigment.factories.AppContext;
import gr.hexad.assigment.service.PaymentService;
import gr.hexad.assigment.util.AppConstants;
import gr.hexad.assigment.util.AppUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class PaymentRulesTest {
   private static RuleConfigLoader rulesConfig;

    @BeforeAll

    public static void setup(){
        rulesConfig = AppContext.getRulesConfigLoader();

    }

    @Test
    @DisplayName("Given there are rules defined for each payment types")
    public void loadPaymentPostProcessRulesTest() throws IncorrectPaymentPostProcessorTypeExeption {

        //create rule config and load rules

        //test rules loaded successfully
         Assertions.assertNotNull(rulesConfig);
        //more tests
       PaymentInfo paymentInfo = new PaymentInfo();
       paymentInfo.setPaymentType(PaymentType.BOOK);
       List<PaymentPostProcessStrategy> strategies = rulesConfig.loadPostProcessRules().get(paymentInfo.getPaymentType());
     

        //Check mapping correct
       Assertions.assertEquals(strategies.size(),3);

       paymentInfo.setPaymentType(PaymentType.PHYSICAL_PRODUCT);
        strategies = rulesConfig.loadPostProcessRules().get(paymentInfo.getPaymentType());
       Assertions.assertEquals(strategies.size(),2);

       //expect exception if wrong payment type
        Assertions.assertThrows(IncorrectPaymentPostProcessorTypeExeption.class,()->AppContext.getPostProcessStrategyInstance(PaymentPostprocessTypes.INVALID_PROCESS));

    }
    @Test
    @DisplayName("Given if payment is create new member activate member and send activation email ")
    public void paymentTypeCreateNewMemberTest() throws MemberNotFoundException, IncorrectPostPaymentPostProcessFailed, URISyntaxException {
        int memberCountBeforAdd  = MemberDAO.memberCount();
        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setPaymentType(PaymentType.NEW_MEMBERSHIP);
        Member member = new Member();
        member.setCustomerName("Chris flayr");
        member.setCustomerEmail("chris112@gmail.com");
        paymentInfo.setMemberInfo(member);

        PaymentService paymentService = AppContext.getPaymentService();
        paymentService.processPayment(paymentInfo);
        List<String> result =paymentService.paymentPostProcess(paymentInfo);
        //Test member ship activated

        Assertions.assertNotEquals(memberCountBeforAdd,MemberDAO.memberCount());

        //Test activation member message

        Assertions.assertTrue(result.get(0).contains(AppConstants.MEMBER_CREATE_SUCCESS_MESSAGE));
        LocalDate membershipExpiry= LocalDate.now().plusMonths(1);

        //Test member expiry
        Assertions.assertTrue(paymentInfo.getMemberInfo().getMembershipExpiry().equals(membershipExpiry));

        //Test activation email template
        Assertions.assertNotNull(result.get(1));
        //Test email template contain member info
        Assertions.assertTrue(result.get(1).contains(member.getCustomerName()));


    }

    @Test
    @DisplayName("Given if payment type update member send member update email and update membership")
    public void paymentTypeUpdateMembershipTest() throws MemberNotFoundException, IncorrectPostPaymentPostProcessFailed, URISyntaxException {


        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setPaymentType(PaymentType.MEMBERSHIP_UPGRDE);
        Member member = new Member();
        member.setCustomerEmail("iftikhar@hotmail.com");

        LocalDate memberExpiryBeforeUpdate = MemberDAO.getCustomer("iftikhar@hotmail.com").getMembershipExpiry();
        paymentInfo.setMemberInfo(member);
        PaymentService paymentService = AppContext.getPaymentService();
        paymentService.processPayment(paymentInfo);
        List<String> result =paymentService.paymentPostProcess(paymentInfo);

        Assertions.assertNotNull(result.get(0));

        //Member expiry updated
        Assertions.assertNotEquals(paymentInfo.getMemberInfo().getMembershipExpiry(),memberExpiryBeforeUpdate);
        Assertions.assertTrue(paymentInfo.getMemberInfo().getMembershipExpiry().equals(memberExpiryBeforeUpdate.plusMonths(1)));
    }

    @Test
    @DisplayName("Given if payment type is Book generate packing slip and  duplicate slip and commission payment ")
    public void testPaymentTypeBook() throws MemberNotFoundException, IncorrectPostPaymentPostProcessFailed, URISyntaxException {

        double commissionPayment = 15 * AppConstants.COMMISSION_RATE;

        Assertions.assertEquals(AppUtils.calculateCommisionPayment(15),commissionPayment);
        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setPaymentType(PaymentType.BOOK);
        paymentInfo.setAmount(15);
        paymentInfo.setMemberInfo(MemberDAO.getCustomer("iftikhar@hotmail.com"));
        PaymentService paymentService = AppContext.getPaymentService();
        List<String> result = paymentService.paymentPostProcess(paymentInfo);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.size(),3);
        paymentInfo.setPaymentType(PaymentType.PHYSICAL_PRODUCT);


        Assertions.assertTrue(result.get(1).contains(AppConstants.DUPLICATE_PACKING_SLIP_HEADING_TEXT));

        result = paymentService.paymentPostProcess(paymentInfo);

        Assertions.assertEquals(result.size(),2);




    }


}


